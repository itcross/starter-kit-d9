#!/usr/bin/env bash

## Install/Reinstall profile
##
## Usage: fin install-profile

#: exec_target = cli

# Abort if anything fails
set -e

# PROJECT_ROOT and DOCROOT are set as env variables in cli
SITE_DIRECTORY="default"
DOCROOT_PATH="${PROJECT_ROOT}/${DOCROOT}"
SITEDIR_PATH="${DOCROOT_PATH}/sites/${SITE_DIRECTORY}"

source ./.docksal/commands/helper/echo-style.sh
source ./.docksal/commands/helper/install.sh

# Install Profile
profile_install ()
{
  echo-green "Installing from the profile ..."
	cd $DOCROOT_PATH
	# We disable email sending here so site-install does not return an error
	# Credit: https://www.drupal.org/project/phpconfig/issues/1826652#comment-12071700
  drush site-install itcross_starter_kit -y \
		install_configure_form.enable_update_status_module=NULL \
		--account-name=AdminTech \
		--site-name='ITCross Starter Kit'
}

config_ignore ()
{
  echo-green "Importing the 'ignore' configuration split ..."
  drush csim ignore -y
}

# Post Install
post_install ()
{
  echo-green "Post install actions ..."
  echo "Running cron ..."
	cd $DOCROOT_PATH
	drush cron
	cd ${PROJECT_ROOT}
}

# Update database with `drush updb` command
execute_profile_hooks ()
{
  echo-green "Updating database ..."
  cd $DOCROOT_PATH
  drush ev "drupal_set_installed_schema_version('itcross_starter_kit', 8000)"
	drush updb -y
}

# Profile initialization steps
echo-yellow "\nStep 1 ..."
source ./.docksal/commands/set-permissions

echo-yellow "\nStep 2 ..."
cd $DOCROOT_PATH
composer_install

echo-yellow "\nStep 3 ..."
init_settings "${PROJECT_ROOT}/local/docroot/sites/default/settings.local.prod.php"

echo-yellow "\nStep 4 ..."
profile_install

# Todo: echo-yellow "\nStep 5 ..."
# Todo: execute_profile_hooks

# Todo: echo-yellow "\nStep 6 ..."
# Todo: config_ignore

# Todo: echo-yellow "\nStep 7 ..."

# Todo:
#if [[ "$@" =~ "-nd" ]]; then
#  echo-yellow "'-nd' parameter is present. Devel mode setting up is skipped. "
#else
#  cd ${PROJECT_ROOT}
#  source ./.docksal/commands/set-dev-mode 1
#fi

# Todo: echo-yellow "\nStep 8 ..."
# Todo: cd ${PROJECT_ROOT}
# Todo: source ./.docksal/commands/gulp

# Todo: echo-yellow "\nStep 9 ..."
# Todo: post_install

echo -e "\nOpen ${yellow}http://${VIRTUAL_HOST}${NC} in your browser to verify the setup."
