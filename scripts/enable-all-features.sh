#!/usr/bin/env bash

drush en -y itcross_devel # Because there are functions that can be used in the features

drush en -y itcross_itc_advanced_content
drush fim -y itcross_itc_advanced_content
drush fim -y itcross_itc_advanced_content # Twice, because one time is not enough
drush en -y itcross_itc_banner
drush fim -y itcross_itc_banner
drush en -y itcross_itc_service
drush fim -y itcross_itc_service
drush en -y itcross_itc_people
drush fim -y itcross_itc_people
drush en -y itcross_itc_portfolio
drush fim -y itcross_itc_portfolio
drush en -y itcross_itc_contact
drush fim -y itcross_itc_contact
drush en -y itcross_itc_dashboard
drush fim -y itcross_itc_dashboard
drush en -y itcross_itc_product
drush fim -y itcross_itc_product
drush en -y itcross_itc_commerce
drush fim -y itcross_itc_commerce
