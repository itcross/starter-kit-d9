#!/usr/bin/env bash

## Install/Reinstall profile
##
## Usage: install-profile

# Abort if anything fails
set -e

source ./.docksal/commands/helper/echo-style.sh
source ./.docksal/commands/helper/install.sh

# Install Profile
profile_install ()
{
  echo-green "Installing from the profile ..."
	# We disable email sending here so site-install does not return an error
	# Credit: https://www.drupal.org/project/phpconfig/issues/1826652#comment-12071700
	drush site-install itcross_starter_kit -y \
		install_configure_form.enable_update_status_module=NULL \
		--account-name=AdminTech \
		--site-name='ITCross Starter Kit'
}

config_ignore ()
{
  echo-green "Importing the 'ignore' configuration split ..."
  drush csim ignore -y
}

# Post Install
post_install ()
{
  echo-green "Post install actions ..."
  echo "Running cron ..."
	drush cron
}

# Update database with `drush updb` command
execute_profile_hooks ()
{
  echo-green "Updating database ..."
  drush ev "drupal_set_installed_schema_version('itcross_starter_kit', 8000)"
	drush updb -y
}

# Profile initialization steps
echo-yellow "\nStep 1 ..."
composer_install

echo-yellow "\nStep 3 ..."
profile_install

echo-yellow "\nStep 4 ..."
execute_profile_hooks

echo-yellow "\nStep 5 ..."
config_ignore

echo-yellow "\nStep 6 ..."
bash scripts/devops/gulp.sh

echo-yellow "\nStep 7 ..."
post_install

echo-green "\nCompleted."
